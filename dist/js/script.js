const formEntry = document.querySelector(".modal-form");
formEntry.addEventListener("submit", (e) => {
  e.preventDefault();
  const inputLogin = document.querySelector("#login-input");
  const passInput = document.querySelector("#pass-input");
  login(inputLogin.value, passInput.value);

  return false;
});

const cardSection = document.querySelector(".card-section-a");
const tokenKey = "stepProjectCardsToken";
function login(email = "mail", password = "password") {
  fetch(`https://ajax.test-danit.com/api/v2/cards/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password }),
  })
    .then((response) => response.text())
    .then((token) => {
      localStorage.setItem(tokenKey, token);
      if (
        localStorage.getItem("stepProjectCardsToken") === null ||
        localStorage
          .getItem("stepProjectCardsToken")
          .includes("Validation failed") ||
        localStorage
          .getItem("stepProjectCardsToken")
          .includes("Incorrect username or password")
      ) {
        logIn.style.display = "block";
        buttonCreate.style.display = "none";
        modal.style.display = "block";
      } else {
        logIn.style.display = "none";
        modal.style.display = "none";
        buttonCreate.style.display = "block";
      
        renderAllCards();
      }
      return token;
    });
}

function init() {
  if (
    localStorage.getItem("stepProjectCardsToken") === null ||
    localStorage
      .getItem("stepProjectCardsToken")
      .includes("Validation failed") ||
    localStorage
      .getItem("stepProjectCardsToken")
      .includes("Incorrect username or password")
  ) {
    document.querySelector(".button-entry").style.display = "block";
    document.querySelector(".button-create").style.display = "none";
  } else {
    document.querySelector(".button-entry").style.display = "none";
    document.querySelector(".button-create").style.display = "block";
      renderAllCards();
  }
}
init();

const select = document.querySelector(".select");
//console.log(select);
select.addEventListener("change", (e) => {
  const formItems = document.querySelector(".aded-form_items");
  if (e.target.value === "cardiologist") {
    formItems.innerText = "";
    formItems.insertAdjacentHTML(
      "beforeEnd",
      `<div class="form-floating">
    <input
    id = 'pressure'
      type="text"
      class="form-control rounded-3"
      placeholder="Звичайний тиск
      "
      required
    />
  </div>
  <div class="form-floating">
  <input
  id = 'body-mass'
    type="text"
    class="form-control rounded-3"
    placeholder="Індекс маси тіла"
    required
  />
</div>
<div class="form-floating">
<input
id = 'diseases'
  type="text"
  class="form-control rounded-3"
  placeholder="Перенесені захворювання серцево-судинної системи"
  required
/>
</div>
<div class="form-floating">
<input
id = 'age'
  type="text"
  class="form-control rounded-3"
  placeholder="Вік"
  required
/>
</div>`
    );
  }
  if (e.target.value === "dentist") {
    formItems.innerText = "";
    formItems.insertAdjacentHTML(
      "beforeEnd",
      `<div class="form-floating">
    <input
    id = 'last-visit'
      type="date"
      class="form-control rounded-3"
      placeholder="Дата останнього відвідування
      "
      required
    />
  </div>`
    );
  }
  if (e.target.value === "therapist") {
    formItems.innerText = "";
    formItems.insertAdjacentHTML(
      "beforeEnd",
      `<div class="form-floating">
    <input
    id = 'age'
      type="text"
      class="form-control rounded-3"
      placeholder="Вік"
      required
    />
  </div>`
    );
  }
  if (e.target.value === "") {
    formItems.innerText = "";
  }
});

class Visit {
  constructor(cardData) {
    this.id = cardData.id;
    this.name = cardData.name;
    this.visitName = cardData.visitName;
    this.visitDiscription = cardData.visitDiscription;
    this.urgency = cardData.urgency;
    this.doctor = cardData.doctor;
  }
}

class VisitCardiologist extends Visit {
  constructor(cardData) {
    super(cardData);
    this.normalPressure = cardData.normalPressure;
    this.bodyMassIndex = cardData.bodyMassIndex;
    this.diseases = cardData.diseases;
    this.age = cardData.age;
    this.htmlBuildCardCardiologist();
  }

  htmlBuildCardCardiologist() {
    cardSection.insertAdjacentHTML(
      "afterbegin",

      `<div id="${this.id}" class="cards-container" data-doctor=${this.doctor}  >
 <div class="card" >
                <div class="card__close"></div>
                <div class="card-box">
                <p class="card__text name">${this.name}</p>
                  <p class="card__text">Кардіолог</p>
                  <div class="card__but-wrapper">
                    <button
                      class="card__but-wrapper-button"
                    >
                      Узнать больше
                    </button>
                    <button class="card__but-wrapper-button-reduction">
                      Редактировать
                    </button>
                  </div>
                </div>
              </div>

      <div class="card--active ">
                <div class="card__close"></div>
                <div class="card-box">
                  <p class="card__text">Кардіолог</p>
                  <div class="card-special-info">
                  <label for="card__text">Ім'я</label>
                    <p class="card__text ">${this.name}</p>
                    <label for="card__text">Мета візиту</label>
                    <p class="card__text">${this.visitName}</p>
                    <label for="card__text">Опис</label>
                    <p class="card__text">${this.visitDiscription}</p>
                    <label for="card__text">Терміновість</label>
                      <p class="card__text">${this.urgency}</p>
                      
                    </div>
                  <div class="card-special-info-wrapper">
                    <div class="card__but-wrapper--active">
                      <button
                        class="card__but-wrapper-button"
                      >
                        Вернуться
                      </button>
                      <button class="card__but-wrapper-button-reduction">
                        Редактировать
                      </button>
                    </div>
                    <div class="card-special-info">

                    <label for="card__text">Вік</label>
                      <p class="card__text">${this.age}</p>
                      <label >Нормальний тиск</label>
                      <p class="card__text">${this.normalPressure}</p>
                      <label for="card__text">Індекс маси тіла</label>
                      <p class="card__text">${this.bodyMassIndex}</p>
                      <label for="card__text">Перенесені захворювання серцево-судинної системи</label>
                      <p class="card__text">${this.diseases}</p>
                      
                     </div>
                  </div>
                </div>
              </div>
           </div>
`
    );
  }
}

class VisitDentist extends Visit {
  constructor(cardData) {
    super(cardData);
    this.lastDate = cardData.lastDate;
    this.htmlBuildCardDentist();
  }

  htmlBuildCardDentist() {
    cardSection.insertAdjacentHTML(
      "afterbegin",
      `<div id="${this.id}" class="cards-container" data-doctor=${this.doctor} >
         <div class="card" >
                <div class="card__close"></div>
                <div class="card-box">
                <p class="card__text name">${this.name}</p>
                  <p class="card__text">Стоматолог</p>
                  <div class="card__but-wrapper">
                    <button 
                      class="card__but-wrapper-button"
                    >
                      Узнать больше
                    </button>
                    <button class="card__but-wrapper-button-reduction">
                      Редактировать
                    </button>
                  </div>
                </div>
              </div>

              <div class="card--active" >
                <div class="card__close"></div>
                <div class="card-box">
                <p class="card__text">Стоматолог</p>
                <div class="card-special-info">
                <label for="card__text">Ім'я</label>
                  <p class="card__text ">${this.name}</p>
                  <label for="card__text">Мета візиту</label>
                  <p class="card__text">${this.visitName}</p>
                  <label for="card__text">Опис</label>
                  <p class="card__text">${this.visitDiscription}</p>
                  <label for="card__text">Терміновість</label>
                    <p class="card__text">${this.urgency}</p>
                    
                  </div>
                  <div class="card-special-info-wrapper">
                    <div class="card__but-wrapper--active">
                      <button
                        class="card__but-wrapper-button"
                      >
                        Вернуться
                      </button>
                      <button class="card__but-wrapper-button-reduction">
                        Редактировать
                      </button>
                    </div>
                    <div class="card-special-info">
                    <label for="card__text">Дата останнього візиту</label>
                    <p class="card__text">${this.lastDate}</p>
                  </div>
               </div>
           </div>
          
        </div>      
`
    );
  }
}

class VisitTerapevt extends Visit {
  constructor(cardData) {
    super(cardData);
    this.age = cardData.age;
    this.htmlBuildCardTerapevt();
  }

  htmlBuildCardTerapevt() {
    cardSection.insertAdjacentHTML(
      "afterbegin",
      `<div id="${this.id}" class="cards-container" data-doctor=${this.doctor} >
      
         <div class="card" >
                <div class="card__close"></div>
                <div class="card-box">
                <p class="card__text name">${this.name}</p>
                  <p class="card__text">Терапевт</p>
                  <div class="card__but-wrapper">
                    <button 
                      class="card__but-wrapper-button"
                    >
                      Узнать больше
                    </button>
                    <button class="card__but-wrapper-button-reduction">
                      Редактировать
                    </button>
                  </div>
                </div>
              </div>
             
              <div class="card--active" >
                <div class="card__close"></div>
                <div class="card-box">
                  <p class="card__text">Терапевт</p>
                  <div class="card-special-info">
                  <label for="card__text">Ім'я</label>
                    <p class="card__text ">${this.name}</p>
                    <label for="card__text">Мета візиту</label>
                    <p class="card__text">${this.visitName}</p>
                    <label for="card__text">Опис</label>
                    <p class="card__text">${this.visitDiscription}</p>
                    <label for="card__text">Терміновість</label>
                      <p class="card__text">${this.urgency}</p>
                      
                    </div>
                  <div class="card-special-info-wrapper">
                    <div class="card__but-wrapper--active">
                      <button
                        class="card__but-wrapper-button "
                      >
                        Вернуться
                      </button>
                      <button class="card__but-wrapper-button-reduction">
                        Редактировать
                      </button>
                    </div>
                    <div class="card-special-info">
                      <p class="card__text">${this.age}</p>
                      </div>
                  </div>
                </div>
        </div>      
`
    );
  }
}
class Modal {
  constructor(
    name,
    visitDiscription,
    visitName,
    urgency,
    doctor,
    normalPressure,
    bodyMassIndex,
    diseases,
    age,
    lastDate
  ) {
    this.name = name;
    this.visitDiscription = visitDiscription;
    this.visitName = visitName;
    this.urgency = urgency;
    this.doctor = doctor;
    this.normalPressure = normalPressure;
    this.bodyMassIndex = bodyMassIndex;
    this.diseases = diseases;
    this.age = age;
    this.lastDate = lastDate;
  }
}

const logIn = document.querySelector(".button-entry");
let modal = document.querySelector(".container");
const cardsSection = document.querySelector(".card-section");
logIn.addEventListener("click", () => {
  modal.style.display = "block";
  cardsSection.style.display = "none";
});
const closeBtnlog = document.querySelector(".close-img_2");
closeBtnlog.addEventListener("click", () => {
  modal.style.display = "none";
  cardsSection.style.display = "flex";
});

const buttonCreate = document.querySelector(".button-create");
let modalCreateVisit = document.querySelector(".container-registration");
buttonCreate.addEventListener("click", () => {
  modalCreateVisit.style.display = "block";
  cardsSection.style.display = "none";
});

const closeBtn = document.querySelector(".close-img");
closeBtn.addEventListener("click", () => {
  modalCreateVisit.style.display = "none";
  cardsSection.style.display = "flex";
});
const reset = document.getElementById("reset");
function showFilter() {
  const btnFilter = document.querySelector(".btn-filter-main");
  btnFilter.addEventListener("click", () => {
    const filterBox = document.querySelector(".filter-box");
    filterBox.style.display = "none";
    // filterDoctor();

    if (filterBox.style.display !== "block") {
      filterBox.style.display = "block";
    } else {
      filterBox.style.display = "none";
    }
  });
}

showFilter();

const noItems = document.querySelector(".no-items");
function newVisitCard() {
  const btnCreateVisit = document.querySelector(".btn-visit");
  btnCreateVisit.addEventListener("click", (e) => {
    e.preventDefault();

    const valueDoctor = document.getElementById("doctors").value;
    const InputvisitName = document.getElementById("input-theme").value;
    const visitDescription = document.getElementById("visi-discription").value;
    const selectUrgensy = document.getElementById("urgency").value;
    const inputName = document.getElementById("input-name").value;
    const newVisit = new Modal();
    newVisit.doctor = valueDoctor;
    newVisit.visitDiscription = visitDescription;
    newVisit.visitName = InputvisitName;
    newVisit.urgency = selectUrgensy;
    newVisit.name = inputName;
    if (valueDoctor === "cardiologist") {
      const inputPressure = document.getElementById("pressure").value;
      const inputBodyMass = document.getElementById("body-mass").value;
      const inputDiseases = document.getElementById("diseases").value;
      const inputAge = document.getElementById("age").value;
      newVisit.normalPressure = inputPressure;
      newVisit.bodyMassIndex = inputBodyMass;
      newVisit.diseases = inputDiseases;
      newVisit.age = inputAge;
    } else if (valueDoctor === "dentist") {
      const inputLastVisit = document.getElementById("last-visit").value;
      newVisit.lastDate = inputLastVisit;
    } else if (valueDoctor === "therapist") {
      const inputAge = document.getElementById("age").value;
      newVisit.age = inputAge;
    }
    console.log(newVisit);
    postCard(newVisit);
    modalCreateVisit.style.display = "none";
    noItems.style.display = "none";
  });
}
newVisitCard();

function postCard(newVisitDan) {
  fetch(`https://ajax.test-danit.com/api/v2/cards`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("stepProjectCardsToken")}`,
    },
    body: JSON.stringify(newVisitDan),
  })
    .then((response) => response.json())

    .then((response) => renderCard(response));
}

function renderCard(post) {
  if (post.doctor === "therapist") {
    new VisitTerapevt(post);
    
  } else if (post.doctor === "dentist") {
    new VisitDentist(post);
  } else if (post.doctor === "cardiologist") {
    new VisitCardiologist(post);
  }
  // console.log(post);
  
  showMores();
  deleteCard(post);
  noItems.style.display = "none";
  

  // console.log(post)
}

async function getData(url) {
  const fetchedUrl = await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("stepProjectCardsToken")}`,
    },
  });
  const data = await fetchedUrl.json();
  return data;
}

async function renderAllCards() {
  const postData = await getData(`https://ajax.test-danit.com/api/v2/cards`);
  const array = postData.forEach((post) => {
    if (post.doctor === "therapist") {
      new VisitTerapevt(post);
    } else if (post.doctor === "dentist") {
      new VisitDentist(post);
    } else if (post.doctor === "cardiologist") {
      new VisitCardiologist(post);
    }
    noItems.style.display = "none";

    showMores();
    deleteCard(post);
 
  });
}
async function showMores() {
  let showMoreBtn = document.querySelectorAll(".card__but-wrapper-button");
  //console.log(showMoreBtn);
  showMoreBtn.forEach((e) => {
    e.addEventListener("click", () => {
      console.log("jjj");
      let container = e.closest(".cards-container");
      let cards = container.children[0];
      let activeCards = container.children[1];
      cards.classList.toggle("passive");
      activeCards.classList.toggle("active");
    });

  });
}

function del(cardId) {
  fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("stepProjectCardsToken")}`,
    },
  });
}

async function deleteCard(post) {
  const delCardBtn = document.querySelectorAll(`.card__close`);
  delCardBtn.forEach((e) => {
    e.addEventListener("click", () => {
      del(post.id);
      e.parentNode.remove();
    });
  });
}



function filter() {
  const doctorName = document.querySelector(".doctor-name");
  const radioBtns = document.querySelectorAll('input[name="doctor-name"]');
  function filterDoctor() {
    doctorName.addEventListener("click", () => {
      updateFilterVaule();
      for (const radioButton of radioBtns) {
        if (radioButton.checked) {
          let selectedSize = radioButton.id;
          const arrNotFilter = document.querySelectorAll(
            `div[data-doctor="${selectedSize}"]`
          );
          Array.from(arrNotFilter).map((e) => {
            e.style.display = "block";
          });
        } else {
          let selectedSize = radioButton.id;

          const arrNotFilter = document.querySelectorAll(
            `div[data-doctor="${selectedSize}"]`
          );
          Array.from(arrNotFilter).map((e) => {
            e.style.display = "none";
          });
        }
      }
    });
  }


function updateFilterVaule() {
  reset.disabled = false;
}

function defaultFilter() {
  reset.addEventListener("click", () => {
    radioBtns.forEach((e) => {
      e.click();
      e.checked = true;
    });
    pacientNameStr = "";
    reset.disabled = true;
  });
}

filterDoctor();

function filterPacient() {
  const nameFilter = document.querySelector(".name-pacient-input-search");
  nameFilter.addEventListener("input", () => {
    updateFilterVaule();
    const pacientNameStr = nameFilter.value;
    const defaultFilterPatient = nameFilter.value;
    // const value = pacientNameStr;
    const allFilterNames = document.querySelectorAll(".name");
    Array.from(allFilterNames).forEach((pacient) => {
      if (
        pacient.innerText.toLowerCase().includes(pacientNameStr.toLowerCase())
      ) {
        pacient.closest(".cards-container").style.display = "block";
      } else {
        pacient.closest(".cards-container").style.display = "none";
      }
    });
  });
  defaultFilter();
}
filterPacient();
}

filter()